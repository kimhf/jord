jQuery(document).ready(function($){
	function reverse(s){
		return s.split("").reverse().join("");
	}	
	function str_rot13(str) {
	  return (str + '').replace(/[a-z]/gi, function (s) {
		return String.fromCharCode(s.charCodeAt(0) + (s.toLowerCase() < 'n' ? 13 : -13));
	  });
	}
	
	$('.cursorium').each(function(){
		var a = $(this).data('a');
		var b = a.split("").sort().join("");
		var c = $(this).data('c');
		var d = "";
		var q = $(this).data('q');

		for ( var e = 0; e < c.length; e++ ) {
			d+=b.charAt(a.indexOf(c.charAt(e)));
		}
		d = str_rot13(d);
		
		var eil = reverse(str_rot13(q))+'@'+reverse(d);
		
		$(this).replaceWith('<a href="mailto:'+eil+'">'+eil+'</a>');
	});
});