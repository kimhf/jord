<?php
/*
Plugin Name: Jord
Plugin URI:
Description:
Version: 0.0.31
Author:
Author URI:
Text Domain:
License:
*/

/**
 * Jord includes
 *
 * The $jord_includes array determines the code library included in the plugin.
 *
 * Missing files will produce a fatal error.
 */

function jord_includes() {
	$jord_includes = [
	  'lib/email-and-phone.php',		// email and phone functions
	];

	foreach ($jord_includes as $file) {
	  $filepath = dirname(__FILE__) . '/' . $file;

	  require_once $filepath;
	}
	unset($file, $filepath);
}
jord_includes();

function jord_after_setup_theme() {

  define( 'JORD_DIR_URL', plugin_dir_url( __FILE__ ) );
  define( 'JORD_DIR', dirname(__FILE__) );

  global $Jord;
  $Jord = new Jord;

  require_if_theme_supports( 'jord-relative-urls', JORD_DIR . '/lib/relative-urls.php' );
  require_if_theme_supports( 'jord-disable-emoji-support', JORD_DIR . '/lib/emoji.php' );
  require_if_theme_supports( 'jord-hide-post-type-post', JORD_DIR . '/lib/post-type-post.php' );
  require_if_theme_supports( 'jord-prevent-skype-links', JORD_DIR . '/lib/post-type-attachment.php' );
  require_if_theme_supports( 'jord-hide-comments-system', JORD_DIR . '/lib/comments.php' );
  require_if_theme_supports( 'jord-prevent-skype-links', JORD_DIR . '/lib/skype.php' );
  require_if_theme_supports( 'jord-admin-bar-remove-wp-logo', JORD_DIR . '/lib/admin-bar-menu-wp-logo.php' );
  require_if_theme_supports( 'jord-nice-search', JORD_DIR . '/lib/nice-search.php' );
  require_if_theme_supports( 'jord-cleanup', JORD_DIR . '/lib/cleanup.php' );
  require_if_theme_supports( 'jord-jquery-cdn', JORD_DIR . '/lib/jquery-cdn.php' );
  require_if_theme_supports( 'jord-defer-scripts', JORD_DIR . '/lib/defer-scripts.php' );
  require_if_theme_supports( 'jord-whitelabel-system-email', JORD_DIR . '/lib/whitelabel-system-email.php' );
  require_if_theme_supports( 'jord-whitelabel-login', JORD_DIR . '/lib/whitelabel-login.php' );
  require_if_theme_supports( 'jord-custom-svg-logo', JORD_DIR . '/lib/custom-svg-logo.php' );
  require_if_theme_supports( 'jord-defer-stylesheets', JORD_DIR . '/lib/defer-stylesheets.php' );
  require_if_theme_supports( 'jord-inline-bjll', JORD_DIR . '/lib/inline-bjll.php' );

  if ( current_theme_supports('custom-logo') ) {
    require_once JORD_DIR . '/lib/custom-logo-output.php';
  }
}
add_action( 'after_setup_theme', 'jord_after_setup_theme', 999 );

class Jord {
  var $logo = [];

  function __construct() {
    $this->init_logos();
  }

  function init_logos() {
    $logo = [];
    if ( has_custom_logo() ) {
      $custom_logo_id = get_theme_mod( 'custom_logo' );
      $attachment_image_src = wp_get_attachment_image_src( $custom_logo_id , 'full' );

      $logo['img'] = $attachment_image_src[0];
      $logo['width'] = $attachment_image_src[1];
      $logo['height'] = $attachment_image_src[2];

      if ( current_theme_supports('jord-custom-svg-logo') ) {
        $logo['svg'] = get_option('custom_svg_logo');

        /*$jord_custom_svg_logo = get_theme_support( 'jord-custom-svg-logo' );
        if ( is_array( $jord_custom_svg_logo ) && isset( $jord_custom_svg_logo['inline'] ) && $jord_custom_svg_logo['inline'] ) {

        }*/
      }
    } else {
      $logo = get_theme_support( 'jord-custom-default-logo' );
      if ( is_array($logo) ) {
        $logo = $logo[0];
      }
    }
    if ( !empty( $logo ) ) {
      $this->logo = array_replace_recursive(
        ['type'=>'',
         'width'=>'',
         'height'=>'',
         'img'=>'',
         'svg'=>'',
         'svgdir'=>'',
         'alt'=>''],
        $logo);
    }
  }

  function get_logo_src() {
    return $this->logo;
  }
}

function jord_get_logo_src() {
  global $Jord;
  return $Jord->get_logo_src();
}
