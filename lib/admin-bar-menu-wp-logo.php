<?php
namespace Jord\Lib\BarLogo;

function remove_wp_logo( $wp_admin_bar ) {
  
	$wp_admin_bar->remove_node( 'wp-logo' );
  
}

add_action( 'admin_bar_menu', __NAMESPACE__ . '\\remove_wp_logo', 999 );