<?php
namespace Jord\Lib\Comments;

function admin_head() {
  /**
   * Hide 'Turn comments on or off' link from dashboard Welcome to WordPress! widget
   */
  echo '<style>.welcome-icon.welcome-comments {display: none;}</style>';
}

/**
 * Set comment count to 0 to hide statistics on dashboard "At a glance" widget
 */
function wp_count_comments( $count ) {
  $count = [
    'approved'            => 0,
    'awaiting_moderation' => 0,
    'spam'                => 0,
    'trash'               => 0,
    'post-trashed'        => 0,
    'total_comments'      => 0,
    'all'                 => 0,
    'moderated'           => 0,
  ];

  return (object) $count;
}

/**
 * Remove comments links from admin bar
 */
function admin_bar_menu( $wp_admin_bar ) {
  $wp_admin_bar->remove_node( 'comments' );
}

function init() {
  if ( is_admin() ) {
    add_action( 'admin_head', __NAMESPACE__ . '\\admin_head' );

    add_filter( 'wp_count_comments', __NAMESPACE__ . '\\wp_count_comments', 10, 2 );
  }

  if ( is_admin_bar_showing() ) {
    add_action( 'admin_bar_menu', __NAMESPACE__ . '\\admin_bar_menu', 999 );
  }
}
init();

/**
 * Disable support for comments and trackbacks in post types
 */
function disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ( $post_types as $post_type ) {
		if ( post_type_supports( $post_type, 'comments' ) ) {
			remove_post_type_support( $post_type, 'comments' );
			remove_post_type_support( $post_type, 'trackbacks' );
		}
	}
}
add_action('admin_init', __NAMESPACE__ . '\\disable_comments_post_types_support');

/**
 * Close comments on the front-end
 */
function disable_comments_status() {
	return false;
}
add_filter('comments_open', __NAMESPACE__ . '\\disable_comments_status', 20, 2);
add_filter('pings_open', __NAMESPACE__ . '\\disable_comments_status', 20, 2);

/**
 * Hide existing comments
 */
function disable_comments_hide_existing_comments( $comments ) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', __NAMESPACE__ . '\\disable_comments_hide_existing_comments', 10, 2);


function disable_comments_admin_menu() {
  // Remove comments page in menu
  remove_menu_page('edit-comments.php');

  //Remove “Discussion” sub-menu page from “Settings”
  remove_submenu_page( 'options-general.php', 'options-discussion.php' );
}
add_action('admin_menu', __NAMESPACE__ . '\\disable_comments_admin_menu');

/**
 * Redirect any user trying to access comments page
 */
function disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', __NAMESPACE__ . '\\disable_comments_admin_menu_redirect');

/**
 * Remove comments metabox from dashboard
 */
function disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', __NAMESPACE__ . '\\disable_comments_dashboard');

?>
