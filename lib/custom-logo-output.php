<?php
namespace Jord\Lib\CustomDefaultLogo;

function custom_logo_output( $html ) {

  $logo = \jord_get_logo_src();

  if ( is_customize_preview() ) {
    if ( strpos( $html, 'style="display:none;"' ) !== false && ! empty( $logo['img'] ) ) {
      $html = '';
    }
  }

  $jord_custom_svg_logo = get_theme_support( 'jord-custom-svg-logo' );

  if ( is_array( $jord_custom_svg_logo ) && isset( $jord_custom_svg_logo[0]['inline'] ) && $jord_custom_svg_logo[0]['inline'] ) {
    if ( ! empty( $logo['svgdir'] ) && file_exists( $logo['svgdir'] ) ) {
      $svg = file_get_contents( $logo['svgdir'] );

      $html = sprintf( '<a href="%1$s" class="custom-logo-link inline-svg-link" rel="home">%2$s</a>',
        esc_url( home_url( '/' ) ),
        $svg
      );
    }
  } else {
    if ( empty( $html ) ) {
      if ( ! empty( $logo ) ) {
        $html = sprintf( '<a href="%1$s" class="custom-logo-link" rel="home">%2$s</a>',
          esc_url( home_url( '/' ) ),
          '<img width="'.$logo['width'].'" height="'.$logo['height'].'" src="'.$logo['img'].'" class="custom-logo" alt="'.$logo['alt'].'">'
        );
      }
    }

    if ( ! empty( $html ) && ! empty( $logo['svg'] ) ) {
      $html = str_replace( 'src="', 'src="' . $logo['svg'] . '" onerror="this.src=\'', $html );

      $html = str_replace( '.png"', '.png\'"', $html );
      $html = str_replace( '.jpg"', '.jpg\'"', $html );
    }
  }
  return $html;
}
add_filter( 'get_custom_logo', __NAMESPACE__ . '\\custom_logo_output', 10 );
