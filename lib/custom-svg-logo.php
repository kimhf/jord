<?php
namespace Jord\Lib\CustomSvgLogo;

function customize_register( $wp_customize ) {
  $wp_customize->add_setting('custom_svg_logo', array(
      'capability'        => 'edit_theme_options',
      'type'           => 'option',
  ));

  $wp_customize->add_control( new \WP_Customize_Upload_Control($wp_customize, 'custom_svg_logo', array(
      'label'    => __('SVG Logo', 'jord'),
      'section'  => 'title_tagline',
      'settings' => 'custom_svg_logo',
  )));
}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

function mime_types( $mimes ) {
  if ( current_user_can( 'manage_options' ) ) {
    $mimes['svg'] = 'image/svg+xml';
  }
  return $mimes;
}
add_filter( 'upload_mimes', __NAMESPACE__ . '\\mime_types' );