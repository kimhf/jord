<?php

/**
 * Mark most .js as defer.
 */
add_filter('script_loader_tag', function( $tag, $handle, $src ) {
	if ( is_admin() ) {
        return $tag;
	}

	if (in_array($handle, [
		'jquery-core',
		'jquery-migrate'
	])) {
		return $tag;
	}

	if ( FALSE !== strpos( $tag, ' defer' ) || FALSE !== strpos( $tag, ' async' ) ) {
		return $tag;
	}

	if ( FALSE !== strpos( $src, 'modernizr' ) || FALSE !== strpos( $src, 'advanced-custom-fields' ) ) {
		return $tag;
	}

	return preg_replace( '/^<script /i', '<script defer="defer" ', $tag );
}, 20, 3 );
