<?php
/** 
 *	Lock up emails JS (add to javascript file)
 */

function init_fix_emails_filter() {

	wp_register_script( 'jord-cursorium', JORD_DIR_URL . 'assets/js/jord-cursorium.js' , ['jquery'], '0.0.4', true );

	if (! is_user_logged_in() && ! isset($_GET['show-reset-form'])) {
		add_filter( 'the_content', 'get_fixed_emails', 1000 );
		add_filter( 'widget_text', 'get_fixed_emails', 1000 );
	}
}
add_action( 'init', 'init_fix_emails_filter' );

/**
 * Automatically hide email adresses from spambots on your WordPress blog
 */
function get_fixed_emails($content) {
	$pattern = '/(<a .*)href=(")mailto:([^"]*)"([^>]*)>([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})<\/a>/U';
	
	$content = preg_replace_callback($pattern,"security_remove_emails_atags", $content);

    $pattern = '/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})/i';//
	
    $content = preg_replace_callback($pattern,"security_remove_emails_logic", $content);

    return $content;
}

function security_remove_emails_atags($subject) {
	$pattern = '/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})/i';
	preg_match($pattern, $subject[3], $matches, PREG_OFFSET_CAPTURE, 3);
	if ( !empty( $matches ) ) {
		return $subject[3];
	}
	return '';
}

function security_remove_emails_logic($result) {

	wp_enqueue_script( 'jord-cursorium' );

	return get_email_super_antispambot( $result[0] );
}

/**
 *	Try to hide email from web crawlers, http://www.maurits.vdschee.nl/php_hide_email/ + antispambot();, Javascript in seperate file, Css to display corectly before mouse over event
 */
function get_email_super_antispambot ( $email ) {
	if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
		return $email;
	}
	$emailparts = explode('@',$email);
	$emailFront = $emailparts[0];
	$emailFront = strrev( str_rot13 ( $emailFront ) );
	$emailBack = $emailparts[1];
	$emailBack = strrev( str_rot13 ( $emailBack ) );

	//$character_set = "+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz";
	$character_set = "+-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz";
	$key = str_shuffle($character_set);
	$emailBackCipher = '';
	for ($i=0;$i<strlen($emailBack);$i+=1) $emailBackCipher.= $key[strpos($character_set,$emailBack[$i])];

	return '<span class="cursorium" data-a="'.$key.'" data-c="'.$emailBackCipher.'" data-q="'.antispambot($emailFront).'">[javascript beskyttet e-postadresse]</span>';
}



/** 
 *	Make phone number link on phones, PHP function get_phone_link($telephone); JS
 */
 /*
jQuery(document).ready(function(){
	if ($('.phonenumber').length > 0) {
		if (jQuery.browser.mobile){
			$('.phonenumber').each(function(){
				var callto = $(this).data('callto');
				var telephone = $(this).data('telephone');
				$(this).html('<a href="callto:'+callto+'" title="Ring: '+callto+'">'+telephone+'</a>');
			});
		}
	}
});*/

/**
 *	Find Phone numbers in content and return links
 */
function get_fixed_phone_numbers($content) {

    $pattern = '/[0-9]{8}|[0-9]{7}[\s][0-9]{1}|[0-9]{6}[\s][0-9]{2}|[0-9]{5}[\s][0-9]{3}|[0-9]{4}[\s][0-9]{4}|[0-9]{4}[\s][0-9]{2}[\s][0-9]{2}|[0-9]{3}[\s][0-9]{5}|[0-9]{3}[\s][0-9]{3}[\s][0-9]{2}|[0-9]{3}[\s][0-9]{2}[\s][0-9]{3}|[0-9]{2}[\s][0-9]{6}|[0-9]{2}[\s][0-9]{4}[\s][0-9]{2}|[0-9]{2}[\s][0-9]{3}[\s][0-9]{3}|[0-9]{2}[\s][0-9]{2}[\s][0-9]{2}[\s][0-9]{2}/';
    
	$fix = preg_replace_callback($pattern,"add_phone_link_logic", $content);

    return $fix;
}
function add_phone_link_logic($result) {
	return get_phone_link($result[0]);
}

/**
 *	Phone link
 */
function get_phone_link( $telephone ){
	$remove = array("(", ")", " ");
	$callto = str_replace($remove, "", $telephone);
	if (0 === strpos($callto, '+')) {
		return '<span class="phonenumber" data-callto="'. $callto.'" data-telephone="'. $telephone.'">'. $telephone.'</span>';
	} else {
		preg_match_all('/[0-9]{8}/', $callto, $matches);
		if ( !empty($matches[0]) ) {
			return '<span class="phonenumber" data-callto="+47'. $callto.'" data-telephone="'. $telephone.'">'. $telephone.'</span>';
		}
		return $telephone;
	}
}