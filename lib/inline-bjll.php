<?php

add_filter('script_loader_tag', function( $tag ) {
	if ( is_admin() || FALSE === strpos( $tag, '/bj-lazy-load/js/bj-lazy-load.min.js' ) ) { // not our file
			return $tag;
	}

	if (file_exists(WP_PLUGIN_DIR . '/bj-lazy-load/js/bj-lazy-load.min.js')) {
		$script = file_get_contents(WP_PLUGIN_DIR . '/bj-lazy-load/js/bj-lazy-load.min.js');

		if ($script) {
			// CDATA and type='text/javascript' is not needed for HTML 5
			// $tag = "<script type='text/javascript'>\n/* <![CDATA[ */\n{$script}\nif ( typeof BJLL !== 'undefined' && typeof BJLL.check !== 'undefined'){BJLL.check();}\n/* ]]> */\n</script>\n";
			$tag = "<script type='text/javascript'>\n/* <![CDATA[ */\n{$script}\n document.addEventListener('DOMContentLoaded', function() {\n";
			$tag .= "if ( typeof BJLL !== 'undefined' && typeof BJLL.check !== 'undefined'){BJLL.check();}\n";
			$tag .= "});\n/* ]]> */\n</script>\n";
		}
	}

	return $tag;
}, 10, 1 );
