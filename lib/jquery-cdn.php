<?php
/**
 * Enqueue jquery from cdn
 */
add_action('wp_head', function () {
  // Use Google CDN's jQuery if it matches the version from WP.
  // Fallback to local WP core version if offline.
  // It's kept in the header instead of footer to avoid conflicts with plugins.
  $wp_scripts = wp_scripts();

	if ( ! is_admin() && current_theme_supports('jord-jquery-cdn') && isset($wp_scripts->registered['jquery-core']) && $wp_scripts->registered['jquery-core']->ver == '1.12.4') {
    $wp_jquery_path = $wp_scripts->registered['jquery-core']->src;
    $wp_jquery_url = site_url($wp_jquery_path);
    $wp_jquery_url = add_query_arg('ver', $wp_scripts->registered['jquery-core']->ver, $wp_jquery_url);

    $wp_scripts->registered['jquery-core']->src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js';

    wp_add_inline_script( 'jquery-core', 'window.jQuery||document.write(\'<script src="'.esc_url($wp_jquery_url).'"><\/script>\')' );
	}
}, 1);
