<?php
namespace Jord\Lib\Attachments;

/**
 * Change default image link in the media overlay if set to attachment page
 */
function pre_get_posts_admin() {
  $default_link_type = get_option( 'image_default_link_type' );

  if ( $default_link_type == 'post' ) {
    update_option('image_default_link_type', 'none');
  }
}

/**
 * Hide attachment page related elements from wp admin
 */
function admin_head() {
  /**
   * Hide 'Link to post' dropdown option from media overlay
   */
  echo '<style>.attachment-display-settings .link-to option[value="post"] {display: none;}</style>';

  /**
   * Hide 'View attachment page' link from media overlay
   */
  echo '<style>.attachment-info .actions .view-attachment {display: none;}</style>';

  /**
   * Hide 'Link to post' dropdown option from edit media overlay
   */
  echo '<style>.media-modal .setting.link-to option[value="post"] {display: none;}</style>';

  /**
   * Hide 'Link to post' dropdown option from edit gallery overlay
   */
  echo '<style>.media-modal .setting .link-to option[value="post"] {display: none;}</style>';
}

/**
 * Redirect attachment/post pages to parent post or home page
 */
function pre_get_posts_front( $query ) {
  if ( $query->is_main_query() ) {
    // If attachment query
    if ( $query->is_attachment() ) {
      
      $post = get_post( $query->query_vars['attachment'] );           

      /**
       * Redirect to attachment parent (the post it was uploaded to)
       */
      if ( $post != null && is_object( $post ) && ! empty( $post->post_parent ) && is_numeric( $post->post_parent ) && $permalink = get_permalink( $post->post_parent ) ) {
        
      } else {
        $permalink = home_url();
      }

      wp_safe_redirect( $permalink, 301 );
      exit;
    }
  }
}

function init() {
  if ( is_admin() ) {
    add_action( 'admin_head', __NAMESPACE__ . '\\admin_head' );
    add_action( 'pre_get_posts', __NAMESPACE__ . '\\pre_get_posts_admin' );
  } else {
    add_action( 'pre_get_posts', __NAMESPACE__ . '\\pre_get_posts_front' );
  }
}
init();
?>