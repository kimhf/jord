<?php 
namespace Jord\Lib\Posts;

function admin_head() {
  /**
   * Hide 'Add a blog post' link from dashboard 'Welcome to WordPress!' widget
   */
  echo '<style>.welcome-icon.welcome-write-blog {display: none;}</style>';
}

function admin_head_options_reading() {
  /**
   * Hide 'Your latest posts' option from 'options-reading.php'
   */
  echo '<style>#front-static-pages label[for="page_for_posts"],#front-static-pages input[name="show_on_front"], #front-static-pages p, #front-static-pages #front-page-warning {display: none;}
  #front-static-pages ul { margin: 0; }
  </style>';
}

/**
 * Modify default post tupe 'post' to be hidden and not show.
 */
function post_type_posts_init() {
  $post_type = 'post';

  $post_type_object = get_post_type_object($post_type);

  $post_type_object -> public = false;
  $post_type_object -> exclude_from_search = true;
  $post_type_object -> publicly_queryable = false;
  $post_type_object -> show_ui = false;
  $post_type_object -> show_in_menu = false;
  $post_type_object -> show_in_nav_menus = false;
  $post_type_object -> show_in_admin_bar = false;

  register_post_type($post_type, $post_type_object);
}

function safe_redirect_home() {
  $home_url = home_url();
  wp_safe_redirect($home_url);
  exit;
}

/**
 * Remove admin widget metaboxes
 */
function disable_meta_box() {
  /**
   * Remove dashboard widget metabox
   */
  remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
}

/**
 * Remove metaboxes from 'nav-menus.php'
 */
function disable_nav_meta_box() {
  remove_meta_box('add-category', 'nav-menus', 'side');
  remove_meta_box('add-post_tag', 'nav-menus', 'side');
  remove_meta_box('add-post_format', 'nav-menus', 'side');
}

/**
 *  
 */
function pre_option_show_on_front($pre_option, $option) {
  return 'page';
}

/**
 *  
 */
function pre_option_page_for_posts($pre_option, $option) {
  return 0;
}

/**
 *  Set post count to 0 to hide statistics on dashboard "At a glance" widget
 */
function wp_count_posts($counts, $type, $perm) {
  if ($type == 'post') {
    $counts -> publish = 0;
  }
  return $counts;
}

/**
 *  Make sure no posts are returned for dashboard "Activity" widget
 */
function dashboard_recent_posts_query_args($query_args) {
  return [];
}

/**
 *  Redirect post pages to home page
 */
function pre_get_posts_front($query) {
  if ( ! is_admin() && $query->is_main_query() ) {

    // Looks like this might be a single post
    if ( $query->is_singular() && $query->is_single() && ! empty( $query->query_vars['name'] ) && ! $query->get('post_type') && ! $query->is_page() && ! $query->is_attachment() ) {
      add_filter( 'posts_results', __NAMESPACE__.'\\posts_results' );
    }
    
    if ( $query->is_posts_page && $query->is_home && ! is_front_page() ) {
      safe_redirect_home();
    }
    
    // If post type post arcive
    if ( $query->is_archive && ! $query->is_post_type_archive && ! $query->is_home && ! $query->is_tax ) {
      if ( $query->is_date || $query->is_year || $query->is_month || $query->is_day || $query->is_time || $query->is_author || $query->is_category || $query->is_tag ) {
        safe_redirect_home();
      }
    }
  }
}

function posts_results( $posts ) {
  // If single post type post
	if ( ! empty( $posts ) ) { 
    if ( $posts[0]->post_type == 'post' ) {
      safe_redirect_home();
    }
	}
	return $posts;
}

/**
 * Remove posts, tags and categories types from menu customizer
 */
function nav_menu_available_item_types($item_types) {
  foreach($item_types as $key => & $item_type) {
    if ($item_type['type'] == 'taxonomy' && in_array($item_type['object'], ['category', 'post_tag'])) {
      unset($item_types[$key]);
    } else if ($item_type['type'] == 'post_type' && $item_type['object'] == 'post') {
      unset($item_types[$key]);
    }
  }
  return $item_types;
}

function init() {
  if ( is_admin() ) {
    add_action('admin_head', __NAMESPACE__.'\\admin_head');
    add_action('admin_head-options-reading.php', __NAMESPACE__.'\\admin_head_options_reading');

    add_action('admin_init', __NAMESPACE__.'\\disable_meta_box');
    add_action('admin_head-nav-menus.php', __NAMESPACE__.'\\disable_nav_meta_box');

    add_filter('customize_nav_menu_available_item_types', __NAMESPACE__.'\\nav_menu_available_item_types', 10, 1);

    add_filter('wp_count_posts', __NAMESPACE__.'\\wp_count_posts', 10, 3);
    add_filter('dashboard_recent_posts_query_args', __NAMESPACE__.'\\dashboard_recent_posts_query_args', 10, 1);
  } else {
    add_action('pre_get_posts', __NAMESPACE__.'\\pre_get_posts_front');
  }
  add_action('init', __NAMESPACE__.'\\post_type_posts_init');

  add_filter('pre_option_show_on_front', __NAMESPACE__.'\\pre_option_show_on_front', 10, 2);
  add_filter('pre_option_page_for_posts', __NAMESPACE__.'\\pre_option_page_for_posts', 10, 2);
}
init();

/**
 * Redirect any user trying to access posts page
 */
function admin_redirect() {
  global $pagenow;
  if (in_array($pagenow, ['edit.php', 'post-new.php']) && empty($_GET['post_type'])) {
    wp_redirect(admin_url());
    exit;
  } else if ($pagenow === 'post.php' && !empty($_GET['action']) && $_GET['action'] == 'edit' && !empty($_GET['post']) && is_numeric($_GET['post']) && get_post_type($_GET['post']) == 'post') {
    wp_redirect(admin_url());
    exit;
  }
}
//add_action('admin_init', __NAMESPACE__.'\\admin_redirect');
?>