<?php
namespace Jord\Lib\RootRelativeUrls;

/**
 * Root relative URLs
 *
 * WordPress likes to use absolute URLs on everything - let's clean that up.
 * Inspired by http://www.456bereastreet.com/archive/201010/how_to_make_wordpress_urls_root_relative/
 *
 * You can enable/disable this feature in config.php:
 * current_theme_supports('jord-relative-urls');
 *
 */
function root_relative_url($input) {
  preg_match('|https?://([^/]+)(/.*)|i', $input, $matches);

  if (!isset($matches[1]) || !isset($matches[2])) {
    return $input; 
  } elseif (($matches[1] === $_SERVER['SERVER_NAME']) || $matches[1] === $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']) {
    return wp_make_link_relative($input);
  } else {
    return $input;
  }
}

function root_relative_image_src( $image, $attachment_id, $size, $icon ) {
  if ( is_array( $image ) ) {
    $image[0] = root_relative_url($image[0]);
  }

  return $image;
}

function root_relative_image_srcset( $sources, $size_array, $image_src, $image_meta, $attachment_id ) {
  if ( is_array( $sources ) ) {
    foreach ( $sources as &$source ) {
      $source['url'] = root_relative_url($source['url']);
    }
  }

  return $sources;
}

//root-relative-urls
function enable_root_relative_urls() {
  return !(is_admin() || in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'))) && current_theme_supports('jord-relative-urls');
}

if (enable_root_relative_urls()) {

  $root_relative_urls = get_theme_support( 'jord-relative-urls' );

  if ( is_array($root_relative_urls) ) {
    $root_relative_urls = $root_relative_urls[0];
  } else {
    $root_relative_urls = [];
  }

  $root_relative_urls_defaults = [  'root_relative_images' => false,
                                    'root_relative_urls' => true
                                 ];

  $root_relative_urls = array_replace_recursive($root_relative_urls_defaults, $root_relative_urls);

  if ($root_relative_urls['root_relative_urls']) {
    $root_rel_filters = array(
      'bloginfo_url',
      'the_permalink',
      'wp_list_pages',
      'wp_list_categories',
      'roots_wp_nav_menu_item',
      'the_content_more_link',
      'the_tags',
      'get_pagenum_link',
      'get_comment_link',
      'month_link',
      'day_link',
      'year_link',
      'tag_link',
      'the_author_posts_link',
      'script_loader_src',
      'style_loader_src'
    );

    foreach( $root_rel_filters as $root_rel_filter ) {
      add_filter($root_rel_filter, __NAMESPACE__ . '\\root_relative_url');
    }
  }

  if ($root_relative_urls['root_relative_images']) {
    add_filter('wp_get_attachment_image_src', __NAMESPACE__ . '\\root_relative_image_src', 10, 4);
    add_filter('wp_calculate_image_srcset', __NAMESPACE__ . '\\root_relative_image_srcset', 10, 5);
  }
}