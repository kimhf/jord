<?php
/**
 * Enqueue scripts and stylesheets
 *
 * Enqueue stylesheets in the following order:
 * 1. /theme/assets/css/main.min.css
 *
 * Enqueue scripts in the following order:
 * 1. jquery-1.10.2.min.js via Google CDN
 * 2. /theme/assets/js/vendor/modernizr-2.6.2.min.js
 * 3. /theme/assets/js/main.min.js (in footer)
 */
function roots_scripts() {
	// jQuery is loaded using the same method from HTML5 Boilerplate:
	// Grab Google CDN's latest jQuery with a protocol relative URL; fallback to local if offline
	// It's kept in the header instead of footer to avoid conflicts with plugins.
	if ( ! is_admin() && current_theme_supports('jord-jquery-cdn')) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', false, null, false);
		add_filter('script_loader_src', 'roots_jquery_local_fallback', 10, 2);
	}

	/*if (is_single() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}*/
	
	wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'roots_scripts', 100);


// http://wordpress.stackexchange.com/a/12450
function roots_jquery_local_fallback($src, $handle) {
  static $add_jquery_fallback = false;

  if ($add_jquery_fallback) {
    echo '<script>window.jQuery || document.write(\'<script src="' . plugins_url( 'jord/assets/js/1.11.3-jquery.min.js' ) . '"><\/script>\')</script>' . "\n";
    $add_jquery_fallback = false;
  }

  if ($handle === 'jquery') {
    $add_jquery_fallback = true;
  }

  return $src;
}

/*function add_async_forscript( $url ) {
    if ( strpos( $url, '#asyncload' ) === false && strpos( $url, '#deferload' ) === false ) {
        return $url;
    } else if ( is_admin() ) {
        return str_replace('#asyncload', '', $url);
    } else {
		$search  = array( '#asyncload', '#deferload' );
		$replace = array( "' async='async", "' defer='defer" );
		if ( strpos( $url, '.css' ) === false ) {
			return str_replace( $search, $replace, $url);
		} else {
			return str_replace( $search, '', $url);
		}
	}
}*/
//add_filter('clean_url', 'add_async_forscript', 11, 1);

// Adapted from https://gist.github.com/toscho/1584783
add_filter( 'clean_url', function( $url ) {

    if ( is_admin() || FALSE === strpos( $url, '.js' ) ) { // not our file
        return $url;
    } else if ( FALSE !== strpos( $url, 'jquery.js' ) || FALSE !== strpos( $url, 'jquery.min.js' ) ) {
		return $url;
	}
	
    // Must be a ', not "!
    return "$url' defer='defer";
}, 11, 1 );

/*add_filter( 'clean_url', function( $url )
{
    if ( FALSE === strpos( $url, '.css' ) )
    { // not our file
        return $url;
    }
    // Must be a ', not "!
    return "$url' async='async";
}, 11, 1 );*/