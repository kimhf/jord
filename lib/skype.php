<?php
namespace Jord\Lib\Skype;

/**
 * Try to prevent skype to formating phone number links
 */
add_action('wp_head', function () {
	wp_add_inline_script('jquery-migrate', "
		jQuery(document).ready(function() {
			window.setTimeout(function() {
				jQuery('.skype_pnh_container').html('');
				jQuery('.skype_pnh_print_container').removeClass('skype_pnh_print_container');
			}, 800);
		});
	");
}, 1);
