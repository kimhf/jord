<?php
namespace Jord\Lib\WhitelabelLogin;

/*
Adds a custom logo to the wp login screen

Optionaly submit a default fallback logo when adding theme support, or change the priority list of what image to use

add_theme_support('jord-whitelabel-login', [
  'img'         => get_template_directory_uri().'/dist/images/logo.png',
  'svg'         => get_template_directory_uri().'/dist/images/logo.svg',
  'priorities'  => ['logo','default','icon']
]);
*/

function login_head() {
  $whitelabel_login = get_theme_support( 'jord-whitelabel-login' );

  if ( is_array($whitelabel_login) ) {
    $whitelabel_login = $whitelabel_login[0];
  } else {
    $whitelabel_login = [];
  }

  $logo = [];

  $whitelabel_login_defaults = [  'img' => '',
                                  'svg' => '',
                                  'priorities' => ['logo','default','icon'] ];

  $whitelabel_login = array_replace_recursive($whitelabel_login_defaults, $whitelabel_login);

  foreach ( $whitelabel_login['priorities'] as $priority ) {
    switch ($priority) {
      case 'logo':
        $logo = \jord_get_logo_src();
        break;
      case 'default':
        if ( ! empty( $whitelabel_login['img'] ) ) {
          $logo = $whitelabel_login;
        }        
        break;
      case 'icon':
        if ( has_site_icon() ) {
          $logo = [];
          $logo['img'] = get_site_icon_url();
        }
        break;
      default:
        # code...
        break;
    }
    if ( ! empty( $logo ) ) {
      break;
    }
  }  

  if ( ! empty( $logo['img'] ) || ! empty( $logo['svg'] ) ) {

    $version = wp_get_theme()->Version;

    echo '<style>h1 a { ';
    if ( ! empty( $logo['img'] ) ) {
      echo 'background-image:url('.$logo['img'].'?ver='.$version.') !important;';
    }
    if ( ! empty( $logo['svg'] ) ) {
      echo 'background-image:url('.$logo['svg'].'?ver='.$version.') !important;';
    }
    echo 'height:84px !important; 
    width:192px !important; 
    background-size: contain !important; 
    background-position: 50% 100% !important;
    }</style>';
  }
  return;
}
add_action('login_head', __NAMESPACE__ . '\\login_head');

function login_headerurl() {
    return get_bloginfo('url');
}
add_filter('login_headerurl', __NAMESPACE__ . '\\login_headerurl');

function login_headertext() {
  return get_option('blogname');
}
add_filter('login_headertext', __NAMESPACE__ . '\\login_headertext');
