<?php
namespace Jord\Lib\WpEmailFrom;

function filter_system_from_mail( $input ) {
    // not the default address, probably a comment notification.
    if ( 0 !== stripos( $input, 'wordpress' ) )
      return $input; // Not auto-generated
        
    /*
     * Change default WordPress email name
     */
    if ( current_filter() === 'wp_mail_from_name' )
      return get_option('blogname');

    /*
     * Change default WordPress email address
     */
    if ( current_filter() === 'wp_mail_from' ) {

      $home_url = get_home_url();
      $from_email = get_generated_email_from_url($home_url);

      if ( is_email($from_email) ) {
        return $from_email;
      }
    }
      
    return $input;
}

add_filter( 'wp_mail_from',      __NAMESPACE__ . '\\filter_system_from_mail' );
add_filter( 'wp_mail_from_name', __NAMESPACE__ . '\\filter_system_from_mail' );

function get_generated_email_from_url( $url ) {
  if ( $url ) {
    $parse = parse_url( $url );

    $domain = get_domain( $parse['host'], false );

    $email = 'noreply@' . $domain;

    if ( is_email($email) ) {
      return $email;
    }
  }
  return false;
}

/**
 * https://gist.github.com/pocesar/5366899
 * @param string $domain Pass $_SERVER['SERVER_NAME'] here
 * @param bool $debug
 *
 * @debug bool $debug
 * @return string
 */
function get_domain( $domain, $debug = false ) {
	$original = $domain = strtolower($domain);

	if (filter_var($domain, FILTER_VALIDATE_IP)) { return $domain; }

	$debug ? print('<strong style="color:green">&raquo;</strong> Parsing: '.$original) : false;

	$arr = array_slice(array_filter(explode('.', $domain, 4), function($value){
		return $value !== 'www';
	}), 0); //rebuild array indexes

	if (count($arr) > 2)
	{
		$count = count($arr);
		$_sub = explode('.', $count === 4 ? $arr[3] : $arr[2]);

		$debug ? print(" (parts count: {$count})") : false;

		if (count($_sub) === 2) // two level TLD
		{
			$removed = array_shift($arr);
			if ($count === 4) // got a subdomain acting as a domain
			{
				$removed = array_shift($arr);
			}
			$debug ? print("<br>\n" . '[*] Two level TLD: <strong>' . join('.', $_sub) . '</strong> ') : false;
		}
		elseif (count($_sub) === 1) // one level TLD
		{
			$removed = array_shift($arr); //remove the subdomain

			if (strlen($_sub[0]) === 2 && $count === 3) // TLD domain must be 2 letters
			{
				array_unshift($arr, $removed);
			}
			else
			{
				// non country TLD according to IANA
				$tlds = array(
					'aero',
					'arpa',
					'asia',
					'biz',
					'cat',
					'com',
					'coop',
					'edu',
					'gov',
					'info',
					'jobs',
					'mil',
					'mobi',
					'museum',
					'name',
					'net',
					'org',
					'post',
					'pro',
					'tel',
					'travel',
					'xxx',
				);

				if (count($arr) > 2 && in_array($_sub[0], $tlds) !== false) //special TLD don't have a country
				{
					array_shift($arr);
				}
			}
			$debug ? print("<br>\n" .'[*] One level TLD: <strong>'.join('.', $_sub).'</strong> ') : false;
		}
		else // more than 3 levels, something is wrong
		{
			for ($i = count($_sub); $i > 1; $i--)
			{
				$removed = array_shift($arr);
			}
			$debug ? print("<br>\n" . '[*] Three level TLD: <strong>' . join('.', $_sub) . '</strong> ') : false;
		}
	}
	elseif (count($arr) === 2)
	{
		$arr0 = array_shift($arr);

		if (strpos(join('.', $arr), '.') === false
			&& in_array($arr[0], array('localhost','test','invalid')) === false) // not a reserved domain
		{
			$debug ? print("<br>\n" .'Seems invalid domain: <strong>'.join('.', $arr).'</strong> re-adding: <strong>'.$arr0.'</strong> ') : false;
			// seems invalid domain, restore it
			array_unshift($arr, $arr0);
		}
	}

	$debug ? print("<br>\n".'<strong style="color:gray">&laquo;</strong> Done parsing: <span style="color:red">' . $original . '</span> as <span style="color:blue">'. join('.', $arr) ."</span><br>\n") : false;

	return join('.', $arr);
}